/**
 * Calculate EZ Pay Payments based on semester.
 */
(function ($) {
  var rowClasses = function(someNumber){
    return (someNumber%2 == 0) ? 'odd' : 'even';
  };
  $(document).ready(function()
    {
        $('#calc').submit(function()
        {
            var title = 'Payment Schedule for ';
            var semester = $('#semester').val();
            var schedule = new Array();
            switch(semester) {
              case 'WI':
                var tips = "For example, if today's date is <strong>December 10</strong>, the next EZ Pay date would be <strong>December 19</strong>.";
                title += 'Winter 2024';
                schedule[0] = { startdate : 'Nov 15', pct : 10, pmts: 4 };
                schedule[1] = { startdate : 'Dec 19', pct : 20, pmts: 3 };
                schedule[2] = { startdate : 'Jan 19', pct : 30, pmts: 2 };
                schedule[3] = { startdate : 'Feb 14', pct : 50, pmts: 1 };
                break;
              case 'SU':
                var tips = "For example, if today's date is <strong>June 10</strong>, the next EZ Pay date would be <strong>June 13</strong>.";
                title += 'Summer 2024';
                schedule[0] = { startdate : 'Apr 20', pct : 10, pmts: 4 };
                schedule[1] = { startdate : 'May 16', pct : 20, pmts: 3 };
                schedule[2] = { startdate : 'Jun 13', pct : 30, pmts: 2 };
                schedule[3] = { startdate : 'Jul 5', pct : 50, pmts: 1 };
                break;
              case 'FA':
                var tips = "For example, if today's date is <strong>June 10</strong>, the next EZ Pay date would be <strong>June 25</strong>.";
                title += 'Fall 2024';
                schedule[0] = { startdate : 'Jun 25', pct : 10, pmts: 5 };
                schedule[1] = { startdate : 'Jul 25', pct : 20, pmts: 4 };
                schedule[2] = { startdate : 'Aug 26', pct : 30, pmts: 3 };
                schedule[3] = { startdate : 'Sept 26', pct : 50, pmts: 2 };
                break;
            }

            var output = '<h3>' + title + '</h3>';
            output += '<p><i>EZPay enrollment deadlines may be different than HFC payment due dates.<i></p>'
            output += '<table>';
            output += '<tr><th>Last Day to Enroll Online</th><th>Percentage Down</th><th># of Payments</th><th>Down Payment</th><th>Installments</th></tr>';
            for (var item in schedule) {
              var downpayment = parseInt($('#tuition').val()) * schedule[item].pct / 100;
              var installment = (parseInt($('#tuition').val()) - downpayment) / schedule[item].pmts;

              output += '<tr class="' + rowClasses(item) + '">';
              output += '<td>' + schedule[item].startdate + '</td>';
              output += '<td>' + schedule[item].pct + '%</td>';
              output += '<td>' + schedule[item].pmts + '</td>';
              output += '<td> $' + downpayment.toFixed(2) + '</td>';
              output += '<td> $' + installment.toFixed(2) + '</td>';
              output += '</tr>';
            }
            output += '</table>';
            output += "<p>To find the right EZ Pay date, look forward from today's date.<br>" + tips + '</p>';
            $('#payment-info').html(output);
            return false;
        });
    });
})(jQuery);
