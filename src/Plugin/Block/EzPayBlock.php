<?php

/**
 * Defines the EZ Pay Calculator Block.
 */
class EzPayBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('EZ Pay Calculator Block'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('EZ Pay Calculator');
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $markup = <<<ENDTEXT
  <form id="calc">
    <fieldset>
      <label for="semester">Semester:</label>
      <select id="semester" />
        <!-- option value="WI">Winter 2024</option -->
        <option value="SU">Summer 2024</option>
        <option value="FA">Fall 2024</option>
      </select>
      <label for="tuition">Tuition:</label>
      <input type="text" id="tuition" />
      <label for="submit"></label>
      <input type="submit" id="submit" value="&nbsp;Calculate!&nbsp;" />
    </fieldset>
  </form>
  <div id="payment-info"></div>
ENDTEXT;

    $output[] = ['#markup' => $markup];
  }
}
